<?php

namespace App\Http\Controllers;

use App\Keyword;
use League\Csv\Reader;
use League\Csv\Statement;

use Illuminate\Http\Request;

class KeywordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $keywords = Keyword::get();
        return view('keywords.index',['keywords'=>$keywords]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'keyword'=>"required_without:keyword_upload|string|unique:keywords",
                'keyword_upload'=>"required_without:keyword|file"
            ]
        );
        if($request->keyword){
            $keyword = new Keyword;
            $keyword->keyword = $request->keyword;
            $keyword->save();
            return redirect()->back()->with(['success'=>"New Keyword Created"]);
        }
        elseif($request->hasFile('keyword_upload')){
            $csv = Reader::createFromPath($request->file('keyword_upload'), 'r');
            $uploaded = 0;
            $created  = 0;
            foreach ($csv as $item) {
                $uploaded++;
                $check =Keyword::where('keyword',$item[0])->first();
                if(!$check){
                    $keyword = new Keyword;
                    $keyword->keyword = $item[0];
                    $keyword->save();                
                    $created++;
                }
            }
            return redirect()->back()->with(['success'=>"$created/$uploaded Keywords created."]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Keyword  $keyword
     * @return \Illuminate\Http\Response
     */
    public function show(Keyword $keyword)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Keyword  $keyword
     * @return \Illuminate\Http\Response
     */
    public function edit(Keyword $keyword)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Keyword  $keyword
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Keyword $keyword)
    {
        $request->validate(
            ['keyword'=>"required|string|unique:keywords,keyword,$keyword->id"]
        );
        $keyword->keyword = $request->keyword;
        $keyword->save();
        return redirect()->back()->with(['success'=>"Keyword Updated"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Keyword  $keyword
     * @return \Illuminate\Http\Response
     */
    public function destroy(Keyword $keyword)
    {
       $keyword->delete();
       return redirect()->back()->with(['success'=>"Keyword Deleted"]);
    }
}
