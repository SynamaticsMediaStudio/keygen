<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

use Illuminate\Http\Request;
use League\Csv\Reader;
use League\Csv\Statement;
use League\Csv\Writer;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function generate(Request $request)
    {
        $request->validate([
            'upload_generate_file'=>'required|file',
        ]);
        if($request->hasFile('upload_generate_file')){
            $csv = Reader::createFromPath($request->file('upload_generate_file'), 'r');
            $keywords = \App\Keyword::get();
            $files = [];
            foreach ($csv as $item) {
                $master =  $item[0];
                $slug = Str::slug($item[0], '-');
                $records = [];
                Storage::disk('local')->put("$slug.csv", '');
                $files[] = "$slug.csv";

                $writer = Writer::createFromPath("root/storage/$slug.csv", 'w+');

                foreach ($item as $slave) {
                    if($slave !== ""){
                        foreach ($keywords as $keyword) {
                            $records[] = [preg_replace('/\s+/', ' ',trim("$keyword->keyword for $slave"))];
                        }
                        foreach ($keywords as $keyword) {
                            $records[] = [preg_replace('/\s+/', ' ',trim("$slave $keyword->keyword"))];
                        }
                    }
                }
                $writer->insertAll($records);
            }
            $zip = new \ZipArchive();
            $zip->open("download.zip", \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

            foreach ($files as $key) {
                $zip->addFile(storage_path($key), $key);
                Storage::disk('local')->delete($key);
            }
            $zip->close();
            return response()->download("download.zip");
        }
    }
}
