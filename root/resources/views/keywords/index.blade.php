@extends('layouts.app')

@section('content')
<div class="container">
    <div class="py-4"></div>
    <div class="row justify-content-center">
        <div class="col-md-3">
            <div class="btn-group">
                <button data-toggle="modal" data-target="#add-keyword" class="btn btn-success btn-sm">Add Keyword</button>
                <button data-toggle="modal" data-target="#bulk-keyword" class="btn btn-success btn-sm">Import Keywords</button>
            </div>
        </div>
        <div class="col-md-9">
            <h4>Keywords</h4>
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th>Keyword</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($keywords as $keyword)
                        <tr>
                            <th>{{$keyword->keyword}}</th>
                            <th>
                                <form action="{{route('keywords.destroy',[$keyword])}}" method="post" class="btn-group">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button data-toggle="modal" data-target="#edit-keyword"  type="button" data-id="{{$keyword->id}}" data-keyword="{{$keyword->keyword}}" class="btn btn-sm">Edit</button>
                                    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                    @csrf
                                </form>
                            </th>
                        </tr>
                    @endforeach
                    @empty($keywords->count())
                        <tr>
                            <th class="text-black-50">No Keywords Added</th>
                        </tr>
                    @endempty
                </tbody>
            </table>
        </div>
    </div>
</div>
@include('keywords.add')
@include('keywords.edit')
@include('keywords.bulk')
@endsection
