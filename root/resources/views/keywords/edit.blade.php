<!-- The Modal -->
<div class="modal" id="edit-keyword">
  <form action="" id="edit-keyword-form" method="POST" class="modal-dialog modal-dialog-centered modal-sm">
    <input type="hidden" name="_method" value="PUT">
    @csrf
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <strong class="modal-title">Edit Keyword</strong>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="form-group">
            <label for="keyword_edit">Keyword</label>
            <input type="text" required name="keyword" id="keyword_edit" class="form-control">
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="submit" class="btn btn-success">Update</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
</form>
</div>
<script>
$(function(){
    $('#edit-keyword').on('show.bs.modal', function (event) {
      var button    = $(event.relatedTarget)
      var id        = button.data('id')
      var keyword   = button.data('keyword')
      var modal     = $(this)
      modal.find('#keyword_edit').val(keyword)
      modal.find('#edit-keyword-form').attr('action',`{{route('keywords.index')}}/${id}`)
    })
})
</script>