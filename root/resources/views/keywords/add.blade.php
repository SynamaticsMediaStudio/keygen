<!-- The Modal -->
<div class="modal" id="add-keyword">
  <form action="{{route('keywords.store')}}" method="POST" class="modal-dialog modal-dialog-centered modal-sm">
    @csrf
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <strong class="modal-title">Add Keyword</strong>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="form-group">
            <label for="keyword">Keyword</label>
            <input type="text" required name="keyword" id="keyword" class="form-control">
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="submit" class="btn btn-success">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
</form>
</div>