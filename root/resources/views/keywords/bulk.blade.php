<!-- The Modal -->
<div class="modal" id="bulk-keyword">
  <form action="{{route('keywords.store')}}" method="POST" class="modal-dialog modal-dialog-centered modal-sm" enctype="multipart/form-data">
    @csrf
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <strong class="modal-title">Bulk Add Keywords</strong>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="form-group">
            <label for="keyword_upload">Import File</label>
            <input type="file" required name="keyword_upload" id="keyword_upload" class="form-control" accept=".xls,.xlsx,.csv">
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="submit" class="btn btn-success">Upload</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
</form>
</div>