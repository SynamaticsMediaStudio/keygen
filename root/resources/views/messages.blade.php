@if (session('success'))
    <div class="alert alert-success">
        <span>{{session('success')}}</span>
    </div>
@endif
@if (session('danger'))
    <div class="alert alert-danger">
        <span>{{session('danger')}}</span>
    </div>
@endif
@if ($errors->any())
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
            <span>{{$error}}</span>
        @endforeach
    </div>
@endif