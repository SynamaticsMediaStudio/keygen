<!-- The Modal -->
<div class="modal" id="generate">
  <form action="{{route('generate')}}" method="POST" class="modal-dialog modal-dialog-centered modal-sm" enctype="multipart/form-data">
    @csrf
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <strong class="modal-title">Generate Keywords</strong>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="form-group">
            <label for="upload_generate_file">Import File</label>
            <input type="file" required name="upload_generate_file" id="upload_generate_file" class="form-control" accept=".csv">
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="submit" class="btn btn-success">Upload</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
</form>
</div>